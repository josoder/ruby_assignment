Rails.application.routes.draw do


  get 'my_page/index'

  # shorten the admin pages url to /admin
  get 'admin' => 'admin#index'


  get 'comment/index'
  # change the session actions url to /login and /logout
  controller :session do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  resources :hours, :courses, :users, :news_feeds
  resources :comments do
    resources :comments
  end

  resources :news_feeds do
    resources :comments
  end

  get 'news_feeds/index'

  root 'news_feeds#index'

end
