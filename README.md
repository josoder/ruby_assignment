# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* An timesheet application for registering working hours for student helpers at Westerdals.
* Version 1.0

### How do I get set up? ###

### Contributions ###

* Joakim Söderstrand
* Mathias Svendsen
* Jens Svendsen Thorbjørnsen

### Run with docker ### 
This application can be run within a docker-container. 
To do so, run the following sequence of commands in the projects dir:
1. docker-compose run web
2. docker-compose run web rails db:setup
3. docker-compose up

To run tests:
docker-compose run web rails test 

### Log in credentials ###
* User: admin@test.com
  password: admin

* User1: test@test.com
  password: test

* User2: test2@test.com
  password: test

### Git repository ### 
* https://bitbucket.org/josoder/ruby_assignment

### Link to deployed application ### 
* https://timesheet-jmj-exam.herokuapp.com/login

