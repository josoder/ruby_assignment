class NewsFeedsController < ApplicationController
  before_action :set_news_feed, only: [:show, :edit, :update, :destroy]
  before_action :is_authorized?, only: [:edit, :update, :destroy]

  # GET /news_feeds
  # GET /news_feeds.json
  def index
    @news_feeds = NewsFeed.all
  end

  # GET /news_feeds/1
  # GET /news_feeds/1.json
  def show
    @comments = Comment.where(news_feed_id: @news_feed.id).order("created_at DESC")
  end

  # GET /news_feeds/new
  def new
    @news_feed = current_user.news_feeds.build
    respond_to do |format|
      format.js
    end
  end

  # GET /news_feeds/1/edit
  def edit
  end


  # POST /news_feeds
  # POST /news_feeds.json
  def create
    @news_feed = current_user.news_feeds.build(news_feed_params)

    respond_to do |format|
      if @news_feed.save
        format.html { redirect_to news_feeds_path}
        format.json { render :show, status: :created, location: @news_feed }
      else
        format.html { render :new }
        format.json { render json: @news_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /news_feeds/1
  # PATCH/PUT /news_feeds/1.json
  def update
    respond_to do |format|
      if @news_feed.update(news_feed_params)
        format.html { redirect_to @news_feed}
        format.json { render :show, status: :ok, location: @news_feed }
      else
        format.html { render :edit }
        format.json { render json: @news_feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /news_feeds/1
  # DELETE /news_feeds/1.json
  def destroy
    @news_feed.destroy
    respond_to do |format|
      format.html { redirect_to news_feeds_url }
      format.json { head :no_content }
    end
  end

  private
  def is_authorized?
    if current_user.id!=@news_feed.user_id&&!current_user.admin
      redirect_to news_feeds_path, notice: "Not authorized" and return
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_news_feed
    @news_feed = NewsFeed.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def news_feed_params
    params.require(:news_feed).permit(:title, :text, :user_id)
  end
end
