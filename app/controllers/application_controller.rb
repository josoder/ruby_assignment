class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include SessionHelper

  before_action :authorize


  protected

  def authorize
    if !is_logged_in?
      redirect_to login_url, notice: "Login"
    end
  end

  def authorized_admin_user
    if !current_user.admin
      redirect_to news_feeds_url, notice: "Not authorized"
    end
  end



end
