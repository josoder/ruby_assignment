class CommentsController < ApplicationController
  before_action :find_news, except: :destroy

  def create
    @comment = @news_feed.comments.create comment_params
    @comment.user = current_user
    if @comment.save
      redirect_to news_feed_url(@news_feed)
    else
      redirect_to news_feed_url(@news_feed), notice: 'Error while creating comment'
    end
  end

  def destroy
    @comment = Comment.find(params[:comment_id])
    @news = NewsFeed.find(@comment.news_feed_id)
    is_authorized?
    if @comment.destroy
      redirect_to news_feed_url(@news)
    else
      redirect_to news_feed_url(@news), notice: "something went wrong"
    end
  end


  private
  def is_authorized?
    if current_user.id!=@comment.user_id&&!current_user.admin
      redirect_to news_feed_url(@news_feed), notice:  "you are not authorized to perform this action" and return
    end
  end

  def comment_params
    params.require(:comment).permit(:text, :user_id)
  end

  def find_news
    @news_feed = NewsFeed.find(params[:news_feed_id])
  end
end
