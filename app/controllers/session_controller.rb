class SessionController < ApplicationController
  skip_before_action :authorize

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.try(:authenticate, params[:password])
      log_in_user(user)
      redirect_to news_feeds_url
    else
      redirect_to login_url, alert: "Incorrect username/password"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to news_feeds_url, notice: "Loged out"
  end
end
