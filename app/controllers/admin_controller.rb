class AdminController < ApplicationController
  before_action :authorized?

  def index
  end

  private
  def authorized?
    if !current_user.admin
      flash[:notice] = "Not authorized"
      redirect_to news_feeds_url
    end
  end
end
