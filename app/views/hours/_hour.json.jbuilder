json.extract! hour, :id, :user_id, :course_id, :date, :hours, :created_at, :updated_at
json.url hour_url(hour, format: :json)
