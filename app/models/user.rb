class User < ApplicationRecord
  # Password hashing
  has_secure_password

  # Destroy hours and news created by the user if the user is deleted
  has_many :hours, dependent: :destroy
  has_many :news_feeds, dependent:  :destroy
  has_many :comments, dependent:  :destroy


  validates  :email, presence: true, uniqueness: true, length: 1..100,
             format: {:with => /.+@.+\..+/i}
  validates :name, presence: true, length: 1..50
  validates :ssnr, presence: true, length: { is: 11 }, numericality: {:only_integer => true}
  validates :street, presence: true, length:  1..100
  validates :zipcode, presence: true, length: 1..10
  validates :city, presence: true, length: 1..30
end
