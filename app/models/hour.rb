class Hour < ApplicationRecord
  belongs_to :user
  belongs_to :course

  validates :user, :course, presence: true
  validates :hours, presence: true, numericality: {greater_than: 0}
end
