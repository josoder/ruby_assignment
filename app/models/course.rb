class Course < ApplicationRecord
  validates :code, :name, presence: true, length: 1..100
end
