class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :news_feed

  validates :text, presence: true
  validates :text, length: 1..1000
end
