class NewsFeed < ApplicationRecord
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :text, presence: true, length: 1..1000
  validates :title, length: 1..100
  validates :user, presence: true
end
