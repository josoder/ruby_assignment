ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all
end

# Need to authorize before the integration tests are run
class ActionDispatch::IntegrationTest
  # Login as user
  def login_as(user)
    post login_path, params: {email: user.email, password: 'secret'}
  end

  def logout
    delete logout_url
  end

  def setup
    login_as users(:admin)
  end

  def second_setup
    login_as users(:two)
  end
end
