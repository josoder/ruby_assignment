require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get users_url
    assert_response :success
  end
  
  test "should not be able to create user without admin account" do
    #logout the admin account and log on as a different user
    logout
    second_setup
    n = User.count
      post users_url, params: { user: { name: @user.name, street: @user.street, zipcode: @user.zipcode,
                                        city: @user.city, email: 'fake@mail.com', password: 'secret', password_confirmation: 'secret', ssnr: @user.ssnr } }

    assert_redirected_to news_feeds_url
    assert(n == User.count)
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { name: @user.name, street: @user.street, zipcode: @user.zipcode,
          city: @user.city, email: 'fake@mail.com', password: 'secret', password_confirmation: 'secret', ssnr: @user.ssnr } }
    end

    assert_redirected_to user_url(User.last)
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { email: @user.email, password: 'secret', password_confirmation: 'secret', ssnr: @user.ssnr } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end
end
