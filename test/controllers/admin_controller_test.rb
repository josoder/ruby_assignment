require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  # should get the admin page when logged on as an admin user
  test "should get index" do
    get admin_url
    assert_response :success
  end

  # should get redirected to the start-page when trying to access admin pages without being authorized
  test "should redirect to news(start)" do
    logout
    second_setup # non admin user login
    get admin_url
    assert_redirected_to(news_feeds_url)
  end

end
