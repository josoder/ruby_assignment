require 'test_helper'
class CommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @news_feed = news_feeds(:one)
    @comment = comments(:one)
  end

  test "should create comment" do
    assert_difference('Comment.count', +1) do
      post news_feed_comments_url(@news_feed), params: {comment: {:text => "test"}}
    end

    assert_redirected_to news_feed_url(@news_feed)
  end

  test "should not allow unauthorized user to delete" do
    # comments count before delete
    n = Comment.count
    # Logout the current user
    logout
    second_setup
    delete '/news_feeds/' + @news_feed.id.to_s, params: {comment: {:id => @comment.id}}
    # should have the same count after the delete is performed
    assert(n==Comment.count)
    end

  test "should delete comment" do
    assert_difference('Comment.count', -1) do
      delete '/news_feeds/' + @news_feed.id.to_s, params: {comment: {:id => @comment.id}}
    end

    assert_redirected_to news_feeds_path
  end
end