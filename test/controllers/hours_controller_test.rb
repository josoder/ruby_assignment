require 'test_helper'

class HoursControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hour = hours(:one)
  end

  test "should get index" do
    get hours_url
    assert_response :success
  end

  test "should create hour" do
    assert_difference('Hour.count') do
      post hours_url, params: { hour: { course_id: @hour.course_id, date: @hour.date, hours: @hour.hours, user_id: @hour.user_id } }
    end

    assert_redirected_to hours_url
  end

  test "should show hour" do
    get hour_url(@hour)
    assert_response :success
  end

  test "should update hour" do
    patch hour_url(@hour), params: { hour: { course_id: @hour.course_id, date: @hour.date, hours: @hour.hours, user_id: @hour.user_id } }
    assert_redirected_to hour_url(@hour)
  end

  test "should destroy hour" do
    assert_difference('Hour.count', -1) do
      delete hour_url(@hour)
    end

    assert_redirected_to hours_url
  end
end
