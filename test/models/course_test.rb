require 'test_helper'

class CourseTest < ActiveSupport::TestCase
  #(loaded from fixtures)
  test "should be valid" do
     course = courses(:one)
     assert course.valid?
   end

  test "should not be valid without course-code" do
    course = courses(:one)
    course.code = nil
    assert_not course.valid?
  end

  test "should not be valid with empty course-code" do
    course = courses(:one)
    course.code = ""
    assert_not course.valid?
  end

  test "should not be valid without name" do
    course = courses(:one)
    course.name = nil
    assert_not course.valid?
  end

  test "should not be valid with empty name" do
    course = courses(:one)
    course.name = ""
    assert_not course.valid?
  end
end
