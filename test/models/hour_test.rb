require 'test_helper'

class HourTest < ActiveSupport::TestCase
  # (loaded from fixtures)
  setup do
    @hour = hours(:one)
  end

  test "should be valid" do
    assert @hour.valid?
  end

  test "should not be valid without user" do
    @hour.user = nil
    assert_not @hour.valid?
  end

  test "should not be valid without course" do
    @hour.course = nil
    assert_not @hour.valid?
  end

  test "should not be valid without hours" do
    @hour.hours = nil
    assert_not @hour.valid?
  end

  test "should not be valid if trying to log 0 hours" do
    @hour.hours = 0
    assert_not @hour.valid?
  end
end
