require 'test_helper'

class NewsFeedTest < ActiveSupport::TestCase
  #(loaded from fixtures)
  setup do
    @news = news_feeds(:one)
  end

  test "the truth" do
     assert @news.valid?
  end

  test "should not be valid without user" do
    @news.user = nil
    assert_not @news.valid?
  end

  test "should not be valid without title" do
    @news.title = nil
    assert_not @news.valid?
  end

  test "should not be valid with empty title" do
    @news.title = ""
    assert_not @news.valid?
  end

  test "should not be valid without text" do
    @news.text = nil
    assert_not @news.valid?
  end

  test "should not be valid with empty text" do
    @news.text = ""
    assert_not @news.valid?
  end
end
