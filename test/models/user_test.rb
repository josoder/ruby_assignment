require 'test_helper'

class UserTest < ActiveSupport::TestCase
  #(loaded from fixtures)
  setup do
    @user = users(:one)
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "should not be valid without email" do
    @user.email = nil
    assert_not @user.valid?
  end

  test "should not be valid with wrongly formated email" do
    @user.email = "jdasdassda@se"
    assert_not @user.valid?
  end

  test "email should be valid" do
    @user.email = "jd@valid.se"
    assert @user.valid?
  end

  test "should not be valid without ssnr" do
    @user.ssnr = nil
    assert_not @user.valid?
  end

  test "should only accept 11 digit ssnr" do
    @user.ssnr = "1337"
    assert_not @user.valid?

    @user.ssnr = "1231231231231231231231"
    assert_not @user.valid?
  end

  test "should not be valid without name" do
    @user.name = nil
    assert_not @user.valid?
  end

  test "should not be valid with empty name" do
    @user.name = ""
    assert_not @user.valid?
  end

  test "should not be valid without street" do
    @user.street = nil
    assert_not @user.valid?
  end

  test "should not be valid with empty street" do
    @user.street = ""
    assert_not @user.valid?
  end

  test "should not be valid without zipcode" do
    @user.zipcode = nil
    assert_not @user.valid?
  end

  test "should not be valid with empty zipcode" do
    @user.zipcode = ""
    assert_not @user.valid?
  end

  test "should not be valid with zipcode with length greater than 10" do
    @user.zipcode = "12314124124124"
    assert_not @user.valid?
  end

  test "shoud not be valid without city" do
    @user.city = nil
    assert_not @user.valid?
  end

  test "should not be valid with empty city" do
    @user.city = ""
    assert_not @user.valid?
  end
end
