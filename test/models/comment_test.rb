require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  # (loaded from fixtures)
  test "should create comment" do
    comment = comments(:one)
    assert comment.valid?
  end

  test "should fail without text" do
    comment = comments(:one)
    comment.text = nil
    assert_not comment.valid?
  end

  test "should fail with empty text" do
    comment = comments(:one)
    comment.text = ""
    assert_not comment.valid?
  end

end
