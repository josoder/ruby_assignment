# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
User.destroy_all
Course.destroy_all
NewsFeed.destroy_all

user1 = User.create(name: "Joakim Söderstrand", ssnr: "11058730123", street: "torshovgata 16b", city: "oslo",
                 zipcode: "0476", password: "test", email: "test@test.com")

user2 = User.create(name: "Mathias Svendsen", ssnr: "11058730123", street: "Skauen", city: "lillestrøm",
                 zipcode: "2000", password: "test", email: "test2@test.com")

user3 = User.create(name: "Jens Svendsen Thorbjørnsen", ssnr: "133713371234", street: "Ivan Bjørndals Gate 23b", city: "Oslo",
                    zipcode: "0472", password: "test", email: "test3@test.com")

admin = User.create(name: "admin user", ssnr: "12345678910", street: "no street", city: "hell",
                    zipcode: "0000", password: "admin", email: "admin@test.com", admin: true)

course1 = Course.create(code: "PG4300", name: "Ruby on Rails")
course2 = Course.create(code: "PG5100", name: "Enterpriseprogrammering 1")

Hour.create([{user_id: user1.id, course_id: course1.id,
               hours: 4, date: Time.new},
             {user_id: user2.id, course_id: course2.id,
              hours: 4, date: Time.new}])

news_feed1 = NewsFeed.create(title:"Velkommen til ett nytt skole år", text:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ac varius nisl. Aenean at augue et ex pretium consectetur.
Duis egestas diam quis enim facilisis, a venenatis velit viverra.", user:user1)

news_feed2 = NewsFeed.create(title:"Det er på tide å dra", text:" quis, consequat at metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas scelerisque turpis sed vehicula ultrice
Duis egestas diam quis enim facilisis, a venenatis velit viverra.", user:user2)



