 class CreateHours < ActiveRecord::Migration[5.0]
  def change
    create_table :hours do |t|
      t.references :user, foreign_key: true
      t.references :course, foreign_key: true
      t.date :date
      t.float :hours

      t.timestamps
    end
  end
end
