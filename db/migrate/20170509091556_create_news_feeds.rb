class CreateNewsFeeds < ActiveRecord::Migration[5.0]
  def change
    create_table :news_feeds do |t|
      t.string :title
      t.string :text
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
